This project originated many years ago as a simple pair of shell
scripts, spawning badblocks(8)
<https://manpages.debian.org/bullseye/e2fsprogs/badblocks.8.en.html>
processes to do reads of every sector on my hard drives, to try to
flag I/O errors before any potential disk failures became more
serious. I also provided a script to query the status of scans in
progress, by looking at the current positions of the relevant file
descriptors in the scanning processes.

I also made use of ionice(1)
<https://manpages.debian.org/bullseye/util-linux/ionice.1.en.html>
to lower the priority of the disk reads, to try to minimize impact
on the system while scans are running.

More recently, I discovered that badblocks(8) has not been updated to
support larger disks (the limit seems to be about 4TiB). So I have
completely rewritten my scripts, now using Python, to do their own
disk reads, and implemented my own protocol for keeping track of
progress via Unix sockets.

Lawrence D'Oliveiro <ldo@geek-central.gen.nz>
2023 June 1
